import 'dart:convert';

import 'package:anime_app/Model/Anime.dart';
import 'package:http/http.dart' as http;

Future<List<Anime>> fetchAnime() async {
  List<Anime> AnimeResult = [];
  print('start');
  var response = await http.get('https://api.jikan.moe/v3/top/anime/1/bypopularity?subtype=bypopularity');
  if (response.statusCode == 200) {
    var data = jsonDecode(response.body);
    final listJson =  data["top"] as List<dynamic>;
    listJson.forEach((element) {
      AnimeResult.add(Anime.fromJson(element));
          });
    return AnimeResult;
  } else {
    print('bug');
    return null;
  }

}
