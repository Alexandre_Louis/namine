import 'dart:ui';
import 'package:anime_app/Model/Anime.dart';
import 'package:anime_app/Model/AnimeModel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'Pages/FavorieAnime.dart';
import 'Pages/TopAnime.dart';

void main() {
  runApp(
    ChangeNotifierProvider(
      create: (context) => AnimeModel(),
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: '鼻の穴',
      theme: ThemeData(),
      home: MyHomePage(title: "Application d'anime "),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: DefaultTabController(
        length: 2,
        child: Scaffold(
            appBar: AppBar(
              backgroundColor: Color.fromRGBO(241, 97, 126, 1.0),
              bottom: TabBar(
                tabs: [
                  Tab(text: 'Top Anime'),
                  Tab(text: 'Mes favoris')
                ],
              ),
            ),
            body: TabBarView(
              children: [
                topAnime(),
                Consumer<AnimeModel>(
                  builder: (context, animesModel, child) {
                    return FavorieAnime(
                      listAnimes: animesModel.animes,
                    );
                  },
                ),
              ],
            )),
      )),
    );
  }
}
