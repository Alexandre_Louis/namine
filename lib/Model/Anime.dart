import 'dart:convert';

class Anime {
  final String NomAnime;
  final int NombreEpisode;
  final String typeDiffusion;
  final String dateDebut;
  final String dateFin;
  final String url;

  final double score;

  Anime({this.NomAnime,
      this.NombreEpisode,
      this.typeDiffusion,
      this.dateDebut,
      this.dateFin,
      this.url,
      this.score});

  factory Anime.fromJson(Map<String, dynamic> json) {
    return Anime(
      NomAnime: json['title'],
      NombreEpisode: json['episodes'],
      typeDiffusion: json['type'],
      dateDebut: json['start_date'],
      dateFin: json['end_date'],
      url: json['image_url'],
      score: json['score'] is int ?json['score'].toDouble():json['score'],

    );
  }



}
