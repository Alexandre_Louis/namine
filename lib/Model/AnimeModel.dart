import 'dart:collection';

import 'package:anime_app/Model/Anime.dart';
import 'package:flutter/cupertino.dart';

class AnimeModel extends ChangeNotifier{
  final Set<Anime> _animes = new Set<Anime>() ;
  UnmodifiableListView<Anime> get animes => UnmodifiableListView<Anime>(_animes);

  void add(Anime anime){
    _animes.add(anime);
    notifyListeners();
  }

  void remove(Anime anime){
    _animes.remove(anime);
    notifyListeners();
  }
}