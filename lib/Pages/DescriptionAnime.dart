import 'package:anime_app/Model/Anime.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DescriptionAnime extends StatelessWidget {
  final Anime anime;

  const DescriptionAnime({Key key, this.anime}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Color.fromRGBO(241, 97, 126, 1.0) ,
      ),
        backgroundColor: Color.fromRGBO(241, 97, 126, 1.0),
        body: Center(
            child:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Hero(
            tag: anime.NomAnime,
            child: Image.network(anime.url),
          ),
          SizedBox(height: 24.0),
          Text(
            anime.NomAnime,
            style: TextStyle(
                fontSize: 32, fontWeight: FontWeight.bold, color: Colors.white),
          ),
          Text(
            "Nombre d'épisode: ${anime.NombreEpisode}",
            style: TextStyle(fontSize: 24, color: Colors.white),
          ),
          Text(
            "Date de début: ${anime.dateDebut}",
            style: TextStyle(fontSize: 22, color: Colors.white),
          ),
          Text(
            "Date de fin: ${anime.dateFin}",
            style: TextStyle(fontSize: 22, color: Colors.white),
          ),

        ])));
  }


}
