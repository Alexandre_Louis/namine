import 'package:anime_app/API/TopAnimeAPI.dart' as API;
import 'package:anime_app/Model/Anime.dart';
import 'package:anime_app/Model/AnimeModel.dart';
import 'package:anime_app/Pages/DescriptionAnime.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class topAnime extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(child: test());
  }

  Widget test() {
    return FutureBuilder<List<Anime>>(
        future: API.fetchAnime(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
              itemCount: 25,
              itemBuilder: (context, index) {
                Anime currentAnime = snapshot.data[index];
                return Card(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[

                      Hero(tag: currentAnime.NomAnime,
                          child: Image.network(currentAnime.url)),
                      ListTile(
                        title: Text(currentAnime.NomAnime,
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.bold)),
                        subtitle: Text(
                          'Support de diffusion : ${currentAnime.typeDiffusion}',
                          style: TextStyle(
                              fontSize: 18, fontStyle: FontStyle.italic),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          TextButton(
                            child: const Text("Plus d'information",
                                style: TextStyle(
                                    fontSize: 18, fontStyle: FontStyle.italic)),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => DescriptionAnime(anime: currentAnime,)),
                              );
                            },
                          ),
                          const SizedBox(width: 8),
                          IconButton(
                            icon: Icon(Icons.favorite_border),
                            onPressed: () {
                              Provider.of<AnimeModel>(context,listen: false).add(currentAnime);
                            },
                          ),
                          const SizedBox(width: 8),
                        ],
                      ),
                    ],
                  ),
                );
              },
            );
          } else if (snapshot.hasError) {
            return Text("Error",
                maxLines: 2,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontStyle: FontStyle.italic,
                    color: Colors.black.withOpacity(0.8)));
          } else {
            return Text('Loading',
                maxLines: 2,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontStyle: FontStyle.italic,
                    color: Colors.black.withOpacity(0.8)));
          }
        });
  }
  
  
}
