import 'package:anime_app/Model/Anime.dart';
import 'package:anime_app/Model/AnimeModel.dart';
import 'package:anime_app/Pages/DescriptionAnime.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FavorieAnime extends StatelessWidget {
  final List<Anime> listAnimes;

  const FavorieAnime({Key key, this.listAnimes}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: listAnimes.length,
      itemBuilder: (context, index) {
        Anime currentAnime = listAnimes[index];
        return Card(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Image.network(currentAnime.url),
              ListTile(
                title: Text(currentAnime.NomAnime,
                    style:
                        TextStyle(fontSize: 22, fontWeight: FontWeight.bold)),
                subtitle: Text(
                  'Support de diffusion : ${currentAnime.typeDiffusion}',
                  style: TextStyle(fontSize: 18, fontStyle: FontStyle.italic),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  TextButton(
                    child: const Text("Plus d'information",
                        style: TextStyle(
                            fontSize: 18, fontStyle: FontStyle.italic)),
                    onPressed: () {
                      Navigator.of(context).push(_AnimationRouteToDescription(currentAnime));
                    },
                  ),
                  const SizedBox(width: 8),
                  IconButton(
                    icon: Icon(Icons.restore_from_trash_rounded),
                    onPressed: () {
                      Provider.of<AnimeModel>(context,listen: false).remove(currentAnime);
                    },),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
  Route _AnimationRouteToDescription(Anime anime) {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) => DescriptionAnime(anime: anime,),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = Offset(0.0, 1.0);
        var end = Offset.zero;
        var curve = Curves.ease;

        var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }
}
